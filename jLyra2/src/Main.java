import java.util.Arrays;

public class Main {

    public static byte[] toNullTerminatedByteArray(String s) {
        byte[] tmp = s.getBytes();
        byte[] bytes = new byte[tmp.length + 1];
        System.arraycopy( tmp, 0, bytes, 0, tmp.length );
        return bytes;
    }

    public static void printByteArray(byte[] array) {
        for (int i = 0; i < array.length; ++i) {
            System.out.print(array[i] + ", ");
        }
    }

    public static int testMain() {
        byte[] password_bytes = toNullTerminatedByteArray(new String("Long PAssword"));
        byte[] salt_bytes = toNullTerminatedByteArray(new String("1234"));
        int time_cost = 1;
        int memory_matrix_rows = 4;
        int memory_matrix_columns = 4;

        byte[] java_output_key = new byte[32];
        byte[] c_output_key = new byte[32];

        new Clyra2().LYRA2(c_output_key, c_output_key.length, password_bytes, password_bytes.length, salt_bytes, salt_bytes.length, time_cost, memory_matrix_rows, memory_matrix_columns);
        Lyra2.LYRA2(java_output_key, password_bytes, salt_bytes, time_cost, memory_matrix_rows, memory_matrix_columns);

        if (Arrays.equals(c_output_key, java_output_key)) {
            System.out.println("[OK] -- Main LYRA2 Test --");
            return 0;
        } else {
            System.out.println("[!!] Main Test FAIL, C (expected) returns differently then Java (actual).");
            System.out.println("Java: " + Arrays.toString(java_output_key));
            System.out.println("C:" + Arrays.toString(c_output_key) + "\n");
            return 1;
        }
    }

    public static int testInitState() {
        long[] Java_state = new long[16];
        long[] C_state = new long[16];

        Sponge.initState(Java_state);
        new Clyra2().initState(C_state);

        if (Arrays.equals(C_state, Java_state)) {
            System.out.println("[OK] Test initState");
            return 0;
        } else {
            System.out.println("[!!] Test initState - FAIL, C (expected) returns differently then Java (actual).");
            System.out.println("Java: " + Arrays.toString(Java_state));
            System.out.println("C: " + Arrays.toString(C_state) + "\n");
            return 1;
        }
    }

    public static int testSqueeze() {
        long[] Java_state = new long[16];
        long[] C_state = new long[16];
        Clyra2 expected = new Clyra2();
        expected.initState(Java_state);
        expected.initState(C_state);

        final int in_len = 100;
        byte[] Java_in = new byte[in_len];
        byte[] C_in = new byte[in_len];

        Sponge.squeeze(Java_state, Java_in, in_len);
        expected.squeeze(C_state, C_in, in_len);

        if (Arrays.equals(C_state, Java_state) && Arrays.equals(C_in, Java_in)) {
            System.out.println("[OK] Test squeeze");
            return 0;
        } else {
            System.out.println("[!!] Test squeeze - FAIL, C (expected) returns differently then Java (actual).");
            System.out.println("Java state: " + Arrays.toString(Java_state));
            System.out.println("C state: " + Arrays.toString(C_state) + "\n");
            System.out.println("Java in: " + Arrays.toString(Java_in));
            System.out.println("C in: " + Arrays.toString(C_in) + "\n");
            return 1;
        }
    }

    public static int testReducedSqueezeRow0() {
        long[] Java_state = new long[16];
        long[] C_state = new long[16];
        Clyra2 expected = new Clyra2();
        expected.initState(Java_state);
        expected.initState(C_state);
        final int in_len = 10;
        byte[] Java_in = new byte[in_len];
        byte[] C_in = new byte[in_len];
        int memory_matrix_rows = 12;
        int memory_matrix_cols = 12;
        long[] Java_wholeMatrix = new long[(int)(memory_matrix_rows * Sponge.BLOCK_LEN_INT64 * memory_matrix_cols)];
        long[] C_wholeMatrix = new long[(int)(memory_matrix_rows * Sponge.BLOCK_LEN_INT64 * memory_matrix_cols)];

        expected.reducedSqueezeRow0(C_state, C_wholeMatrix, memory_matrix_cols);
        Sponge.reducedSqueezeRow0(Java_state, Java_wholeMatrix, memory_matrix_cols);

        if (Arrays.equals(Java_state, C_state) && Arrays.equals(Java_wholeMatrix, C_wholeMatrix)) {
            System.out.println("[OK] Test reducedSqueezeRow0");
            return 0;
        } else {
            System.out.println("[!!] Test reducedSqueezeRow0 - FAIL, C (expected) returns differently then Java (actual).");
            System.out.println("Java state: " + Arrays.toString(Java_state));
            System.out.println("C state: " + Arrays.toString(C_state) + "\n");
            System.out.println("Java wholeMatrix: " + Arrays.toString(Java_wholeMatrix));
            System.out.println("C wholeMatrix: " + Arrays.toString(C_wholeMatrix) + "\n");
            return 1;
        }
    }

    public static int testAbsorbBlock() {
        long[] Java_state = new long[16];
        long[] C_state = new long[16];
        Clyra2 expected = new Clyra2();
        expected.initState(Java_state);
        expected.initState(C_state);
        final int in_len = Sponge.BLOCK_LEN_INT64;
        long[] Java_in = new long[in_len];
        long[] C_in = new long[in_len];

        expected.absorbBlock(C_state, C_in);
        Sponge.absorbBlock(Java_state, Java_in, Sponge.ANY_ARRAY_START);

        if (Arrays.equals(Java_state, C_state) && Arrays.equals(Java_in, C_in)) {
            System.out.println("[OK] Test absorbBlock");
            return 0;
        } else {
            System.out.println("[!!] Test absorbBlock - FAIL, C (expected) returns differently then Java (actual).");
            System.out.println("Java state: " + Arrays.toString(Java_state));
            System.out.println("C state: " + Arrays.toString(C_state) + "\n");
            System.out.println("Java in: " + Arrays.toString(Java_in));
            System.out.println("C in: " + Arrays.toString(C_in) + "\n");
            return 1;
        }
    }

    public static int testAbsorbBlockBlake2Safe() {
        long[] Java_state = new long[16];
        long[] C_state = new long[16];
        Clyra2 expected = new Clyra2();
        expected.initState(Java_state);
        expected.initState(C_state);
        final int in_len = Sponge.BLOCK_LEN_INT64;
        long[] Java_in = new long[in_len];
        long[] C_in = new long[in_len];

        expected.absorbBlockBlake2Safe(C_state, C_in);
        Sponge.absorbBlockBlake2Safe(Java_state, Java_in, Sponge.ANY_ARRAY_START);

        if (Arrays.equals(Java_state, C_state) && Arrays.equals(Java_in, C_in)) {
            System.out.println("[OK] Test absorbBlockBlake2Safe");
            return 0;
        } else {
            System.out.println("[!!] Test absorbBlockBlake2Safe - FAIL, C (expected) returns differently then Java (actual).");
            System.out.println("Java state: " + Arrays.toString(Java_state));
            System.out.println("C state: " + Arrays.toString(C_state) + "\n");
            System.out.println("Java in: " + Arrays.toString(Java_in));
            System.out.println("C in: " + Arrays.toString(C_in) + "\n");
            return 1;
        }
    }

    public static int testReducedBlake2bLyra() {
        long[] Java_state = new long[16];
        long[] C_state = new long[16];
        Clyra2 expected = new Clyra2();
        expected.initState(Java_state);
        expected.initState(C_state);
        Sponge.reducedBlake2bLyra(Java_state);
        expected.reducedBlake2bLyra(C_state);

        if (Arrays.equals(Java_state, C_state)) {
            System.out.println("[OK] Test reducedBlake2bLyra");
            return 0;
        } else {
            System.out.println("[!!] Test reducedBlake2bLyra - FAIL, C (expected) returns differently then Java (actual).");
            System.out.println("Java state: " + Arrays.toString(Java_state));
            System.out.println("C state: " + Arrays.toString(C_state) + "\n");
            return 1;
        }
    }

    public static int testReducedDuplexRowSetup() {
        long[] Java_state = new long[16];
        long[] C_state = new long[16];
        Clyra2 expected = new Clyra2();
        expected.initState(Java_state);
        expected.initState(C_state);
        int memory_matrix_cols = 8;
        int ROW_LEN_INT64 = Sponge.BLOCK_LEN_INT64 * memory_matrix_cols;
        int memory_matrix_rows = 6;
        long[] Java_wholeMatrix = new long[(int)(memory_matrix_rows * ROW_LEN_INT64)];
        long[] C_wholeMatrix = new long[(int)(memory_matrix_rows * ROW_LEN_INT64)];
        int row = 2 * ROW_LEN_INT64;
        int prev = 1 * ROW_LEN_INT64;
        int rowa = 0 * ROW_LEN_INT64;

        expected.reducedDuplexRowSetup(C_state, C_wholeMatrix, prev, rowa, row, memory_matrix_cols);
        Sponge.reducedDuplexRowSetup(Java_state, Java_wholeMatrix, prev, rowa, row, memory_matrix_cols);

        if (Arrays.equals(Java_state, C_state) && Arrays.equals(Java_wholeMatrix, C_wholeMatrix)) {
            System.out.println("[OK] Test reducedDuplexRowSetup");
            return 0;
        } else {
            System.out.println("[!!] Test reducedDuplexRowSetup - FAIL, C (expected) returns differently then Java (actual).");
            System.out.println("Java state: " + Arrays.toString(Java_state));
            System.out.println("C state: " + Arrays.toString(C_state) + "\n");
            System.out.println("Java wholeMatrix: " + Arrays.toString(Java_wholeMatrix));
            System.out.println("C wholeMatrix: " + Arrays.toString(C_wholeMatrix) + "\n");
            return 1;
        }
    }

    public static int testReducedDuplexRow() {
        long[] Java_state = new long[16];
        long[] C_state = new long[16];
        Clyra2 expected = new Clyra2();
        expected.initState(Java_state);
        expected.initState(C_state);
        int memory_matrix_cols = 5;
        int ROW_LEN_INT64 = Sponge.BLOCK_LEN_INT64 * memory_matrix_cols;
        int memory_matrix_rows = 3;
        long[] Java_wholeMatrix = new long[(int)(memory_matrix_rows * ROW_LEN_INT64)];
        long[] C_wholeMatrix = new long[(int)(memory_matrix_rows * ROW_LEN_INT64)];
        int row = 2 * ROW_LEN_INT64;
        int prev = 1 * ROW_LEN_INT64;
        int rowa = 0 * ROW_LEN_INT64;

        expected.reducedDuplexRow(C_state, C_wholeMatrix, prev, rowa, row, memory_matrix_cols);
        Sponge.reducedDuplexRow(Java_state, Java_wholeMatrix, prev, rowa, row, memory_matrix_cols);

        if (Arrays.equals(Java_state, C_state) && Arrays.equals(Java_wholeMatrix, C_wholeMatrix)) {
            System.out.println("[OK] Test reducedDuplexRow");
            return 0;
        } else {
            System.out.println("[!!] Test reducedDuplexRow - FAIL, C (expected) returns differently then Java (actual).");
            System.out.println("Java state: " + Arrays.toString(Java_state));
            System.out.println("C state: " + Arrays.toString(C_state) + "\n");
            System.out.println("Java wholeMatrix: " + Arrays.toString(Java_wholeMatrix));
            System.out.println("C wholeMatrix: " + Arrays.toString(C_wholeMatrix) + "\n");
            return 1;
        }
    }

    public static int testReducedDuplexRow1() {
        long[] Java_state = new long[16];
        long[] C_state = new long[16];
        Clyra2 expected = new Clyra2();
        expected.initState(Java_state);
        expected.initState(C_state);
        int memory_matrix_cols = 5;
        int ROW_LEN_INT64 = Sponge.BLOCK_LEN_INT64 * memory_matrix_cols;
        int memory_matrix_rows = 3;
        long[] Java_wholeMatrix = new long[(int)(memory_matrix_rows * ROW_LEN_INT64)];
        long[] C_wholeMatrix = new long[(int)(memory_matrix_rows * ROW_LEN_INT64)];
        int row = 2 * ROW_LEN_INT64;
        int prev = 1 * ROW_LEN_INT64;

        expected.reducedDuplexRow1(C_state, C_wholeMatrix, prev, row, memory_matrix_cols);
        Sponge.reducedDuplexRow1(Java_state, Java_wholeMatrix, prev, row, memory_matrix_cols);

        if (Arrays.equals(Java_state, C_state) && Arrays.equals(Java_wholeMatrix, C_wholeMatrix)) {
            System.out.println("[OK] Test reducedDuplexRow1");
            return 0;
        } else {
            System.out.println("[!!] Test reducedDuplexRow1 - FAIL, C (expected) returns differently then Java (actual).");
            System.out.println("Java state: " + Arrays.toString(Java_state));
            System.out.println("C state: " + Arrays.toString(C_state) + "\n");
            System.out.println("Java wholeMatrix: " + Arrays.toString(Java_wholeMatrix));
            System.out.println("C wholeMatrix: " + Arrays.toString(C_wholeMatrix) + "\n");
            return 1;
        }
    }

    public static int testPadding() {
        int memory_matrix_cols = 4;
        int ROW_LEN_INT64 = Sponge.BLOCK_LEN_INT64 * memory_matrix_cols;
        int memory_matrix_rows = 4;
        long[] Java_wholeMatrix = new long[(int)(memory_matrix_rows * ROW_LEN_INT64)];
        long[] C_wholeMatrix = new long[(int)(memory_matrix_rows * ROW_LEN_INT64)];
        byte[] password_bytes = toNullTerminatedByteArray(new String("Passwordd"));
        byte[] salt_bytes = toNullTerminatedByteArray(new String("salt"));
        int output_key_len = 128;
        int timeCost = 1;
        Clyra2 expected = new Clyra2();

        expected.padding(output_key_len, timeCost, password_bytes, salt_bytes, memory_matrix_rows, memory_matrix_cols, C_wholeMatrix);
        Lyra2.padding(output_key_len, timeCost, password_bytes, salt_bytes, memory_matrix_rows, memory_matrix_cols, Java_wholeMatrix);

        if (Arrays.equals(Java_wholeMatrix, C_wholeMatrix)) {
            System.out.println("[OK] Test padding");
            return 0;
        } else {
            System.out.println("[!!] Test padding - FAIL, C (expected) returns differently then Java (actual).");
            System.out.println("J wholeMatrix: " + Arrays.toString(Java_wholeMatrix));
            System.out.println("C wholeMatrix: " + Arrays.toString(C_wholeMatrix) + "\n");
            return 1;
        }
    }

    public static void main(String[] args) {
        testInitState();
        testSqueeze();
//        testReducedBlake2bLyra(); DISABLED
        testReducedSqueezeRow0();
        testAbsorbBlock();
        testAbsorbBlockBlake2Safe();
        testReducedDuplexRowSetup();
        testReducedDuplexRow();
        testReducedDuplexRow1();
//        testPadding(); DISABLED
        testMain();
    }
}
