echo 'javac Clyra2.java'
javac Clyra2.java
echo 'javah Clyra2'
javah Clyra2
echo 'export JAVA_HOME=/usr/lib/jvm/java-7-openjdk-amd64/'
export JAVA_HOME=/usr/lib/jvm/java-7-openjdk-amd64/
echo $JAVA_HOME
echo 'gcc -o libclyra2.so -lc -shared -I$JAVA_HOME\include -I$JAVA_HOME\include\linux Lyra2.c Sponge.c Clyra2.c -fPIC'
gcc -o libclyra2.so -lc -shared -I$JAVA_HOME\include -I$JAVA_HOME\include\linux Lyra2.c Sponge.c Clyra2.c -fPIC
