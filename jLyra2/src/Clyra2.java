/**
 * Created by myood on 2015-08-02.
 */
public class Clyra2 {

    public native int LYRA2(byte[] output_key,
                            long output_key_length,
                            byte[] user_password,
                            long user_password_length,
                            byte[] salt,
                            long salt_length,
                            long time_cost, int memory_matrix_rows, int memory_matrix_columns);

    public native void initState(long[] state);

    public native void squeeze(long[] state, byte[] out, int len);

    public native void reducedBlake2bLyra(long[] v);

    public native void reducedSqueezeRow0(long[] state, long[] row_out, long n_cols);

    public native void absorbBlock(long[] state, long[] in);

    public native void absorbBlockBlake2Safe(long[] state, long[] in);

    public native void reducedDuplexRow1(long[] state, long[] matrix, int row_in, int row_out, long n_cols);

    public native void reducedDuplexRow(long[] state, long[] matrix, int row_in, int row_in_out, int row_out, long nCols);

    public native void reducedDuplexRowSetup(long[] state, long[] matrix, int row_in, int row_in_out, int row_out, long n_cols);

    public native int padding(int kLen, int timeCost, byte[] pwd, byte[] salt, int matrix_rows, int matrix_cols, long[] wholeMatrix);
    static {
        System.loadLibrary("clyra2");
    }
}
